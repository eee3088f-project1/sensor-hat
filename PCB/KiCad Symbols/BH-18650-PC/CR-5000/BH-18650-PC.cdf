(part "BH-18650-PC"
    (packageRef "BH18650PC")
    (interface
        (port "1" (symbPinId 1) (portName "-") (portType INOUT))
        (port "2" (symbPinId 2) (portName "+") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "221")
    (property "Manufacturer_Name" "MPD (Memory Protection Devices)")
    (property "Manufacturer_Part_Number" "BH-18650-PC")
    (property "Mouser_Part_Number" "")
    (property "Mouser_Price/Stock" "")
    (property "Arrow_Part_Number" "")
    (property "Arrow_Price/Stock" "")
    (property "Description" "BATTERY HOLDER 18650 PC PIN")
    (property "Datasheet_Link" "http://www.memoryprotectiondevices.com/datasheets/BH-18650-PC-datasheet.pdf")
    (property "symbolName1" "BH-18650-PC")
)
