(part "0402WGF4700TCE"
    (packageRef "RESC1005X40N")
    (interface
        (port "1" (symbPinId 1) (portName "1") (portType INOUT))
        (port "2" (symbPinId 2) (portName "2") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "131")
    (property "Manufacturer_Name" "Uniroyal Elec")
    (property "Manufacturer_Part_Number" "0402WGF4700TCE")
    (property "Mouser_Part_Number" "")
    (property "Mouser_Price/Stock" "")
    (property "Arrow_Part_Number" "")
    (property "Arrow_Price/Stock" "")
    (property "Description" "LEAD-FREE THICK FILM CHIP RESISTORS")
    (property "Datasheet_Link" "https://datasheet.lcsc.com/szlcsc/Uniroyal-Elec-0402WGF4700TCE_C25117.pdf")
    (property "symbolName1" "0402WGF4700TCE")
)
