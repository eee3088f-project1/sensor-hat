*PADS-LIBRARY-PART-TYPES-V9*

TPS61040DBVRG4 SOT95P280X145-5N I ANA 9 1 0 0 0
TIMESTAMP 2022.03.02.21.21.06
"Manufacturer_Name" Texas Instruments
"Manufacturer_Part_Number" TPS61040DBVRG4
"Mouser Part Number" 595-TPS61040DBVRG4
"Mouser Price/Stock" https://www.mouser.co.uk/ProductDetail/Texas-Instruments/TPS61040DBVRG4?qs=Gse6rAGbi78xqRt%252B4OcBkA%3D%3D
"Arrow Part Number" TPS61040DBVRG4
"Arrow Price/Stock" https://www.arrow.com/en/products/tps61040dbvrg4/texas-instruments
"Description" Conv DC-DC 1.8V to 6V Step Up Single-Out 1.8V to 28V 0.4A 5-Pin SOT-23 T/R
"Datasheet Link" https://www.arrow.com/en/products/tps61040dbvrg4/texas-instruments
"Geometry.Height" 1.45mm
GATE 1 5 0
TPS61040DBVRG4
1 0 L SW
2 0 G GND
3 0 L FB
4 0 L EN
5 0 P VIN

*END*
*REMARK* SamacSys ECAD Model
846375/775283/2.49/5/3/Integrated Circuit
