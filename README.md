# Sensor-Hat Main Readme (This readme should be under continuing development as the project is fleshed out.)

## Envirosensing Sensor HAT
Sensor HAT for STM32F Discovery Board.

## Description
The Sensing HAT concept proposed is composed of an accelerometer coupled with a temperature sensor in order to allow for simultaneous motion and heat tracking capability. This would allow for the sensor to be attached to any rotating or moving object, such as a body in motion or a gear/shaft, to gather data on the object’s acceleration/velocity/temperature. This data can then be used for real-time analysis of more complex characteristics such as power dissipation, material warping (as a result of heat and torsion), efficiency of heat shields as well as other variables for system optimization. 

## Getting started! 
Hardware requirements
- An STM32F0 Discovery board to manage and control the Sensing HAT hardware, alternatively another microcontroller structure will do fine.
- A Sensing HAT (You will be creating this from the files in this repo!)
- A TP18650 Battery to power your boarda
- A USB Type B Micro cable

## Software and Tools
- Gitlab - for the purpose of accessing reference documentation.
- KiCad - used to generate the schematic, the PCB layout and finally the gerbers. 
- CubeIDE is particularly useful for coding the STM board as it comes with some built in tools to make the set up much easier but any IDE that supports C++ will be fine. 
-an Oscilloscope is great to test the signals at various points if you need to do some de-bugging. Otherwise, a multi-meter will do. 
- A soldering iron and spare jumper cables 

## Connecting the hardware
 -The battery holder will need to be soldered into place. Be careful to match the positive marking on the holder to the positive marking on the board. 
- The Sensor HAT PCB needs to be connected to the STM32f0 board to access the onboard microcontroller.
 -The footprint for connecting the Sensor Hat to the STM23f0 is incorrect so connections will have to made with leads. Refer to the schematic and image: STM_Hat_Connections in the GitRepo for reference. 
- Once assembled, the Sensor HAT must be powered by a 4.2V Li-ion Battery (Specified to be an 1850). Alternatively, the HAT can be connected to a USB input to power up.
- The USB input is used to access data stored within EEPROM on the HAT.

## Hello World! 
To test the functionality of the Sensor HAT, power it up via connection to a computer using the USB connector. Open the file “Core/SRC/Main” from the Repository and press run. A console window will open displaying the message “OMG, LOOK MA! I DID IT! ” 

To test the board with a new code file. Initialise the file and make sure you select UART1 to be used in the stmCube3D IDE. Scroll down to the main function and copy the following code below any initialization code. 

 
## License 
This project is open source under the Apache License.
Apache License
Version 2.0, January 2004
http://www.apache.org/licenses/
 


## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Will be updated once the board arrives! :)

## Installation
To view kiCad files download the Group35Kicad.zip
(To be included once finished) To view code download the Group35Code.zip

## Usage
Examples of potential use cases involve monitoring data regarding heat output and acceleration of engine crankshafts in order to model the engine operation for optimisation. In addition, the sensor may be used to gather data on the efficiency of heat shields for Spacecrafts undergoing atmosphere re-entry or be used to model wave velocity in order to scout locations for hydroelectric energy production.  It can further be used in conjunction with other motion-related sensors (such as a gyroscope etc.). Since motion and acceleration often generate heat there are many applications for measuring the two in mechanics and industry .

## Support
Get in touch with us via email at:
Julian	: bnkjul001@myuct.ac.za
Zac 	: blnzac001@myuct.ac.za
Jesse	: arnjes009@myuct.ac.za
## Roadmap
Current Status:		
		PCB design complete
Next Steps:
		Testing hardware
		Writing code to log sensor inputs at fixed time intervals

## Contributing
		Welcome to the dev team, so glad that you have ideas to improve this project! Any and all ideas are welcome! (And if you 			would like to point out any flaws in our design, feel free to raise your concerns on Gitlab) 

		If you would like to contribute, please send an email to contributions@SensorHat.ca.za with your idea as well as any 				documentation you have at the ready for your intended changes/additions. A member of the core team will get back to you 			within one business week. If it could be a meaningful contribution (big or small!) we are happy for you to get going with 			development. In this case, please submit a pull request for approval. 

 

		Additionally, once your contribution is finished, please submit a push request for final review.  

		Bugs and Fixes 

		If you come across an issue in the Repo, please log an issue here: 																	https://gitlab.com/eee3088f-project1/sensor-hat/-/issuesnew. 																		Please include any details you feel are relevant including schematics/code etc for potential fixes. 
		
		We will get to it as soon as possible!  

		Format: 

		Any changes to the Schematic and PCB layout must be done in Kicad.  
		Code done in CubeIDE is preferable.  
		Documentation must all be done in British English. 
		

## Authors and acknowledgment
		The main authors are Julian Banks, Zaccary Beilinsohn, Jesse Arendse.
		Big acknowledgment to Brendon Sehlako, Justin Pead and the wonderful EE department ;) 

## Project status
A loving work in progress!
