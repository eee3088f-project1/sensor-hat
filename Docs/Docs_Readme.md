# Docs Readme
This folder contains project information pertaining to:

1. How to use the project.
2. How to manufacture the project.
3. How to maintain and further develop/contribute to the project.

It is also a useful location to store datasheets related to any parts or components used in the design.
